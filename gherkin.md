Ejercicio 1: Revision de escenarios
===================================

Trabajando en grupos:

* ¿Es claro lo que describe el escenario?
* ¿Hay muchos/muy_pocos detalles?
* ¿Utiliza un lenguaje que todos pueden entender?
* ¿Cuán probable es que cambie este escenario cuando cambie la aplicación?


Escenario A: El nombre de usuario debe ser único
-------------------------------------------------

```
Dado que existe un usuario "Juan" en la base de datos
Y navego http://myapp.com
Y cliqueo en "Registrar"
Y completo el "nombre" con "Juan"
Y completo la "clave" con "123456"
Cuando cliqueo en "Enviar"
Entonces el "ResumenDeValidacion" es visible
Y el "ResumenDeValidacion" debe contener "El Nombre elegido ya está en uso"
```

Escenario B: Cambio de la dirección de envío
--------------------------------------------

```
Dado que el enviado una orden
Cuando cambio la dirección de envío
Entonces debería recibir una confirmación
Y la hora estimada de arribo debería ser actualizada
Y la persona encargada del envió debería ser notificada del cambio
```

Escenario C: Cliente recibe notificación de orden
-------------------------------------------------
```
Dado que visito el sitio de Pizza Pepe
Y busco "Pizza Margarita"
Y agregar la pizza al carrito de compras
Y procedo al checkout
Y cargo la dirección de envío
Y cargo los detalles de pago
Cuando confirmo la orden
Entonces debería recibir una notificación
````

Ejercicio 2: Escritura de escenarios
====================================

Regla: la dirección no puede cambiarse una vez que el pedido es recojido
Ejemplo: la orden es recojida durante el proceso de cambio de dirección
* Inicio sesión como Juan
* Ordeno una pizza margarita
* La orden está pendiente de ser recojida
* Comienzo el proceso de cambio de dirección
* Ingreso la nueva dirección "Paseo Colón 850, CABA"
* La orden es recojida por la persona encargada del envío
* Tim confirma el cambio
* La orden es rechazada

Consigna:
1) Escribe el ejemplo precedente en Gherkin
2) Escribe dos escenarios más para esta regla


Checklist para la escritura de escenarios
=========================================

* Concreto: los ejemplos deben usar datos concretos y reales.
* Esecial: todo datos utilizado en los ejemplos deben contribuir al entendimiento del comportamiento que se pretende ilustrar.
* Enfocado: cada ejemplo debe enfocarse en 1 una interacción.
* Interesante: cada ejemplo debe tener una razón de ser. Cada uno debe tener un nombre que releve su intención.
* Declarativo: los ejemplos que declaran lo que el usuario está intentando hacer son más fáciles de entender y mantener que aquellos que describen imperativamente lo que el usuario hace.
* Obicuos: hay que evitar utilizar términos que solo sean entendidos por algunos miembros del equipo. En general es menor evitar lenguaje técnico y utilizar terminología del negocio.


Consejos para la escritura de Gherkin
=====================================

* Una regla por escenario
* Puede resultar práctico comenzar su escritura en orden inverso: Entonces, Cuando, Dado
* Solo un "Cuando" por escenario
* Enfocarse en el "Qué", no en el "Cómo", evitar elementos visuales
* Máximo 5 pasos por escenario
* Nada de emociones (Quiero...)
* Omitir pasos obvios (la aplicación está funcionando)