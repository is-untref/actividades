Definición de casos de prueba
=============================

Definir los casos de prueba para la búsqueda avanzada a implementar en la aplicación Job Vacancy (http://www.jobvacancy.com.ar/) de acuerdo a los siguientes requerimientos:

Filtros:
--------

* location: permite definir una ubicación para la búsqueda, debe devolver todos los resultados a no más de 10 km del punto geográfico. Ejemplo: location:Caseros
* published_after: permite definir una fecha mínima de publicación, debe devolver todos los resultados con fecha de publicación mayor a la definida. Ejemplo: published_after:2018-06-10
* keyword: permite definir una palabra clave de búsqueda, debe devolver todos los resultados que contengan la palabra definida en su título o descripción. Ejemplo:keyword:ruby. Este filtro permite definir múltiples valores, separados por comas y sin espacios, en dicho caso debe devolver los resultados que contengan todas las palabras en su título y descripción. Ejemplo: keyword:ruby,rspec. En este caso devolverá todas las publicaciones que incluyan las palabras ruby Y rspec en su título o descripción.

Consideraciones generales:
--------------------------


* Se puede definir más de un filtro, en dicho caso los espacios en blanco se consideran separadores de filtros. Ejemplo: keyword:java published_after:2018-06-01
* En caso de parámetros de búsqueda que contengan espacios se deben utilizar comillas para que sean considerados como un único parámetro. Ejemplo: location:”Caseros, Buenos Aires”
